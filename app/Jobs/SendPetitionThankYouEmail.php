<?php

namespace App\Jobs;

use App\Entities\Petition;
use App\Entities\Signature;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Message;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendPetitionThankYouEmail implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Petition
     */
    private $petition;

    /**
     * @var Signature
     */
    private $signature;

    /**
     * Create a new event instance.
     * @param Petition $petition
     * @param Signature $signature
     */
    public function __construct(Petition $petition, Signature $signature)
    {
        $this->petition = $petition;
        $this->signature = $signature;
    }

    public function handle(Mailer $mailer)
    {
        $petition = $this->petition;
        $signature = $this->signature;

        $mailer->raw(
            $petition->getEmailBody($signature),
            function (Message $message) use ($petition, $signature) {
                $message->to($signature->email);
                $message->from($petition->user->email, $petition->user->name);
                $message->subject(sprintf('Petition %s: Thank you for your signature', $petition->title));
            }
        );
    }

}