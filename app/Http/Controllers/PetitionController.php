<?php

namespace App\Http\Controllers;

use App\Entities\Petition;
use App\Helpers\Breadcrumb;
use App\Http\Requests\SavePetitionRequest;
use App\Repositories\PetitionRepository;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Concerns\InteractsWithFlashData;
use Illuminate\Http\Request;

class PetitionController extends Controller
{
    use InteractsWithFlashData;

    private $authGuard;

    /**
     * @var PetitionRepository
     */
    private $petitionRepository;

    /**
     * Create a new controller instance.
     * @param Guard $authGuard
     * @param PetitionRepository $petitionRepository
     */
    public function __construct(
        Guard $authGuard,
        PetitionRepository $petitionRepository

    ) {
        $this->middleware('auth');
        $this->authGuard = $authGuard;
        $this->petitionRepository = $petitionRepository;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('client.petitions.index')
            ->with([
                'petitions' => $this->petitionRepository->allForUser($this->authGuard->user()),
                'breadcrumbs' => [
                    new Breadcrumb('Home', route('home')),
                    new Breadcrumb('Client Account', route('client.index')),
                    new Breadcrumb('My Petitions', '', 'active'),
                ]
            ]);
    }

    public function create()
    {
        return view('client.petitions.create')
            ->with([
                'breadcrumbs' => [
                    new Breadcrumb('Home', route('home')),
                    new Breadcrumb('Client Account', route('client.index')),
                    new Breadcrumb('My Petitions', route('client.petitions.index')),
                    new Breadcrumb('Add Petition')
                ]
            ]);
    }

    public function store(SavePetitionRequest $request)
    {
        try {
            $this->petitionRepository->createPetition($request->all(), $this->authGuard->user());
            return redirect()
                ->route('client.petitions.index')
                ->with('success', 'Petition created');
        } catch (\Exception $e) {
            return redirect()
                ->back()
                ->withInput()
                ->with('error', 'Error saving Petition');
        }

    }

    public function edit(Petition $petition)
    {
        return view('client.petitions.edit')
            ->with([
                'petition' => $petition,
                'breadcrumbs' => [
                    new Breadcrumb('Home', route('home')),
                    new Breadcrumb('Client Account', route('client.index')),
                    new Breadcrumb('My Petitions', route('client.petitions.index')),
                    new Breadcrumb(sprintf('Edit petition "%s"', $petition->title))
                ]
            ]);
    }

    public function update(Petition $petition, SavePetitionRequest $request)
    {
        try {
            $this->petitionRepository->save($petition, $request->all());
            return redirect()
                ->route('client.petitions.index')
                ->with('success', 'Petition updated');
        } catch (\Exception $e) {
            return redirect()
                ->back()
                ->withInput()
                ->with('error', 'Error saving Petition');
        }

    }

    public function destroy(Petition $petition)
    {
        try {
            $this->petitionRepository->delete($petition);
            return redirect()
                ->route('client.petitions.index')
                ->with('success', 'Petition deleted');
        } catch (\Exception $e) {
            return redirect()
                ->back()
                ->withInput()
                ->with('error', 'Error saving Petition');
        }
    }

    public function togglePublished(Petition $petition)
    {
        try {
            $petition = $this->petitionRepository->togglePublished($petition);
            return redirect()
                ->route('client.petitions.index')
                ->with('success', 'Petition ' . ($petition->is_published ? 'published' : 'unpublished'));
        } catch (\Exception $e) {
            return redirect()
                ->back()
                ->withInput()
                ->with('error', 'Error toggling published state');
        }
    }

    public function show(Petition $petition)
    {
        return view('client.petitions.show')
            ->with([
                'petition' => $petition,
                'breadcrumbs' => [
                    new Breadcrumb('Home', route('home')),
                    new Breadcrumb('Client Account', route('client.index')),
                    new Breadcrumb('My Petitions', route('client.petitions.index')),
                    new Breadcrumb($petition->title)
                ]
            ]);
    }
}
