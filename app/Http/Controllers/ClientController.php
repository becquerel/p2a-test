<?php

namespace App\Http\Controllers;

use App\Helpers\Breadcrumb;

class ClientController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('client.index')
            ->with([
                'breadcrumbs' => [
                    new Breadcrumb('Home', route('home')),
                    new Breadcrumb('Client Account')
                ]
            ]);
    }
}
