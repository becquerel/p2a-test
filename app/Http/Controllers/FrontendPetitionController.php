<?php

namespace App\Http\Controllers;

use App\Entities\Image;
use App\Entities\Petition;
use App\Exceptions\PetitionNotPublishedException;
use App\Helpers\Breadcrumb;
use App\Http\Requests\SignPetitionRequest;
use App\Repositories\PetitionRepository;
use Illuminate\Database\QueryException;
use Illuminate\Http\Concerns\InteractsWithFlashData;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class FrontendPetitionController extends Controller
{
    use InteractsWithFlashData;

    /**
     * @var PetitionRepository
     */
    private $petitionRepository;

    /**
     * Create a new controller instance.
     * @param PetitionRepository $petitionRepository
     */
    public function __construct(
        PetitionRepository $petitionRepository
    ) {
        $this->petitionRepository = $petitionRepository;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('index')
            ->with([
                'petitions' => $this->petitionRepository->allPublished(),
                'breadcrumbs' => [
                    new Breadcrumb('Home')
                ]
            ]);
    }

    /**
     * @param Petition $petition
     * @param string $slug
     * @return $this
     */
    public function show(Petition $petition, string $slug)
    {
        // if petition is not published, it should not be available publicly and there should be no sign that anything
        // is there at all
        if ($petition->is_published !== true) {
            throw new PetitionNotPublishedException();
        }

        return view('petition.show')
            ->with([
                'petition' => $petition,
                'breadcrumbs' => [
                    new Breadcrumb('Home', route('home')),
                    new Breadcrumb($petition->title),
                ]
            ]);
    }

    /**
     * @param Petition $petition
     * @param SignPetitionRequest|Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sign(Petition $petition, SignPetitionRequest $request)
    {
        try {
            $this->petitionRepository->sign($petition, $request->all());
            return redirect()
                ->route('petition.thankYou', ['petition' => $petition->id, 'slug' => str_slug($petition->title)])
                ->with('success', 'Thank you for your signature');
        } catch (QueryException $e) {
            if ($e->errorInfo[1] == '1062') {   // MySQL code for duplicate entry = petition already signed
                $message = 'It seems that you have already signed this petition before. Thank you.';
            } else {    // other mysql error
                $message = 'Error signing this petition. Please let us know.';
            }
        } catch (\Exception $e) {
            $message = 'Error signing this petition. Please try again or print & sign this on paper';
        }

        return redirect()
            ->back()
            ->withInput()
            ->with('error', $message);
    }

    /**
     * @param Petition $petition
     * @param string $slug
     * @return $this
     */
    public function thankYou(Petition $petition, string $slug)
    {
        if ($petition->is_published !== true) {
            throw new PetitionNotPublishedException();
        }
        return view('petition.thank-you')
            ->with([
                'petition' => $petition,
                'breadcrumbs' => [
                    new Breadcrumb('Home', route('home')),
                    new Breadcrumb(
                        $petition->title,
                        route('petition.show', ['petition' => $petition->id, 'slug' => str_slug($petition->title)])
                    ),
                    new Breadcrumb('Thank You'),
                ]
            ]);
    }

    /**
     * @param Petition $petition
     * @param Image $image
     * @return Response
     */
    public function image(Petition $petition, Image $image)
    {
        return response(file_get_contents($image->getFullPath()))
            ->header('Content-Type', $image->mime);
    }


}
