<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SignPetitionRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|min:3',
            'email' => 'required|email',
            'phone' => 'required|min:10',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Please enter your name',
            'name.min' => 'Your name should have at least 3 letters',
            'email.required' => 'Please enter your email',
            'email.email' => 'This does not seem like valid email address. Please enter your real email address',
            'phone.required' => 'Please enter your phone number',
            'phone.min' => 'Your phone number should have at least 10 characters',
        ];
    }

}