<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SavePetitionRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'title' => 'required|min:10',
            'summary' => 'required',
            'body' => 'required',
            'thank_you_message' => 'required',
            'thank_you_email' => 'required',
            'youtube_url' => 'nullable|regex:/^https:\/\/www\.youtube\.com\/watch\?v=([a-zA-Z0-9]*)$/i'
        ];
    }

    public function messages()
    {
        return [
            'title.min' => 'Petition title should have at least 10 characters',
            'youtube_url.regex' => 'This does not seem to be valid Youtube video URL',
        ];
    }

}