<?php

namespace App\Listeners;

use App\Events\PetitionWasSigned;
use App\Jobs\SendPetitionThankYouEmail;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class PetitionSignedListener
{
    use DispatchesJobs;

    /**
     * Handle the event.
     *
     * @param  PetitionWasSigned $event
     */
    public function handle(PetitionWasSigned $event)
    {
        $petition = $event->getPetition();
        $signature = $event->getSignature();

        $this->dispatch(new SendPetitionThankYouEmail($petition, $signature));
    }
}
