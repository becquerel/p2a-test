<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Class AbstractRespository
 * repository will attempt to forward all not found methods to the repository DB model, so we can automatically use
 * Eloquent model facade
 */
abstract class AbstractRepository
{
    /**
     * @var Model
     */
    protected $modelClass;

    public function __construct()
    {
        // if model class is not explicitly defined on the repository, try to guess it from repository name
        if (empty($this->modelClass)) {
            preg_match('/(.*)Repository/', get_class($this), $matches);
            $this->modelClass = str_ireplace('Repositories', 'Entities', $matches[1]);
        }
        if (!class_exists($this->modelClass)) {
            throw new \UnexpectedValueException(sprintf('Model class %s cannot be found', $this->modelClass));
        }
        $this->model = new $this->modelClass;
    }

    /**
     * magic method to proxy all repository calls to underlying model
     * @param $name
     * @param $arguments
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        return $this->model->$name(...$arguments);
    }

    /**
     * attempts to load model by ID, or returns new instance if fails
     * @param int $id
     * @return mixed
     */
    public function findOrCreate($id)
    {
        try {
            $model = $this->model->findOrFail($id);
            return $model;
        } catch (ModelNotFoundException $e) {
            return new $this->modelClass;
        }
    }

    /**
     * @param Model $model
     * @param array $data
     * @return Model $model
     */
    public function save(Model $model, array $data = [])
    {
        if (count($data) > 0) {
            $model->fill($data);
        }
        $model->save();

        return $model;
    }

    /**
     * @param Model $model
     */
    public function delete(Model $model)
    {
        $model->delete();
    }
}
