<?php

namespace App\Repositories;

class ImageRepository extends AbstractRepository
{
    public function findByFilename(string $filename)
    {
        return $this->model->where('filename', '=', $filename)->get();
    }
}