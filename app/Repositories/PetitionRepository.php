<?php

namespace App\Repositories;

use App\Entities\Image;
use App\Entities\Petition;
use App\Entities\Signature;
use App\Events\PetitionWasSigned;
use App\Exceptions\PetitionNotPublishedException;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;

class PetitionRepository extends AbstractRepository
{
    const ALLOWED_IMAGE_MIME_TYPES = [
        'image/jpeg',
        'image/png',
    ];

    /**
     * @param User $user
     * @return mixed
     */
    public function allForUser(User $user)
    {
        return $this->model->where('user_id', '=', $user->id)->get();
    }

    /**
     * @param array $data
     * @param User $user
     */
    public function createPetition(array $data, User $user)
    {
        $data['user_id'] = $user->id;
        $petition = $this->model->create($data);

        if (isset($data['photo'])) {
            $this->addPhoto($petition, $data['photo']);
        }
    }

    /**
     * toggles published property on a petition
     * @param Petition $petition
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function togglePublished(Petition $petition)
    {
        $petition->is_published = !$petition->is_published;
        return $this->save($petition);
    }

    /**
     * @return Collection
     */
    public function allPublished()
    {
        return $this->model->published(true)->get();
    }

    /**
     * @param Petition $petition
     * @param array $data
     * @throws PetitionNotPublishedException
     */
    public function sign(Petition $petition, array $data)
    {
        if (!$petition->is_published) {
            throw new PetitionNotPublishedException;
        }

        $signature = $petition->signatures()->save(
            new Signature($data)
        );

        //TODO: this might be better injected, instead of using global helper function
        event(new PetitionWasSigned($petition, $signature));
    }

    /**
     * @param Model $model
     * @param array $data
     * @return Model $model
     */
    public function save(Model $model, array $data = [])
    {
        $model = parent::save($model, $data);
        if (isset($data['photo'])) {
            $this->addPhoto($model, $data['photo']);
        }

        return $model;
    }

    /**
     * @param Petition $petition
     * @param UploadedFile $file
     * @todo missing error handling
     */
    private function addPhoto(Petition $petition, UploadedFile $file)
    {
        if ($file->isValid() && in_array($file->getMimeType(), self::ALLOWED_IMAGE_MIME_TYPES)) {
            $this->removeAllPhotos($petition);

            $filename = uniqid('', true) . '.' . strtolower($file->extension());
            $file->storeAs('images', $filename);
            $petition->images()->save(new Image([
                'filename' => $filename,
                'original_filename' => $file->getClientOriginalName(),
                'mime' => $file->getMimeType()
            ]));
        }
    }

    /**
     * remove all images from the petition
     * this would not be in final version, where there would be multiple photos allowed per petition
     * @param Petition $petition
     */
    private function removeAllPhotos(Petition $petition)
    {
        // remove all files from the filesystem
        foreach ($petition->images as $image) {
            if (file_exists($image->getFullPath())) {
                unlink($image->getFullPath());
            }

            $image->delete();
        }
    }
}