<?php

namespace App\Events;

use App\Entities\Petition;
use App\Entities\Signature;
use Illuminate\Queue\SerializesModels;

class PetitionWasSigned
{
    use SerializesModels;

    /**
     * @var Petition
     */
    private $petition;

    /**
     * @var Signature
     */
    private $signature;

    /**
     * Create a new event instance.
     * @param Petition $petition
     * @param Signature $signature
     */
    public function __construct(Petition $petition, Signature $signature)
    {
        $this->petition = $petition;
        $this->signature = $signature;
    }

    /**
     * @return Petition
     */
    public function getPetition(): Petition
    {
        return $this->petition;
    }

    /**
     * @return Signature
     */
    public function getSignature(): Signature
    {
        return $this->signature;
    }

}
