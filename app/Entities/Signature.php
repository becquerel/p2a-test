<?php
namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Signature
 * @package App\Entities
 * @property Petition $petition
 * @property int $petition_id
 * @property string $name
 * @property string email
 * @property string $phone
 *
 */
class Signature extends Model
{
    protected $table = 'signatures';

    protected $fillable = [
        'petition_id',
        'name',
        'email',
        'phone'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function petition()
    {
        return $this->belongsTo(Petition::class);
    }
}
