<?php
namespace App\Entities;

use App\Helpers\HasYoutubeVideo;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class Petition
 * @package App\Entities
 * @property User $user
 * @property int $user_id
 * @property Collection $signatures
 * @property bool $is_published
 * @property string $title
 * @property string $summary
 * @property string $body
 * @property string $thank_you_message
 * @property string $thank_you_email
 * @property string $youtube_url
 * @property Collection $images
 */
class Petition extends Model
{
    use HasYoutubeVideo;

    protected $table = 'petitions';

    protected $fillable = [
        'user_id',
        'is_published',
        'title',
        'summary',
        'body',
        'thank_you_message',
        'thank_you_email',
        'youtube_url'
    ];

    protected $casts = [
        'is_published' => 'boolean',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function signatures()
    {
        return $this->hasMany(Signature::class);
    }

    /**
     * @param Builder $query
     * @param bool $published
     * @return Builder
     */
    public function scopePublished(Builder $query, bool $published = true)
    {
        return $query->where('is_published', '=', $published);
    }

    /**
     * @param string $value
     * @return string
     */
    public function getSummaryAttribute($value)
    {
        return strip_tags($value);
    }

    /**
     * @param string $value
     * @return string
     */
    public function getBodyAttribute($value)
    {
        return strip_tags($value);
    }

    /**
     * @param $value
     * @return string
     */
    public function getThankYouMessageAttribute($value)
    {
        return strip_tags($value);
    }

    /**
     * @param $value
     * @return string
     */
    public function getThankYouEmailAttribute($value)
    {
        return strip_tags($value);
    }

    /**
     * returns email body with replaced placeholders
     * @param Signature $signature
     * @return string
     */
    public function getEmailBody(Signature $signature)
    {
        return str_ireplace(
            ['{petition}', '{name}'],
            [$this->title, $signature->name],
            $this->thank_you_email
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images()
    {
        return $this->hasMany(Image::class);
    }
}
