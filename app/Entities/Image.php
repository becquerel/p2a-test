<?php
namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Signature
 * @package App\Entities
 * @property Petition $petition
 * @property int $petition_id
 * @property string $filename
 * @property string $original_filename
 * @property string $mime
 *
 */
class Image extends Model
{
    protected $table = 'images';

    protected $fillable = [
        'petition_id',
        'filename',
        'original_filename',
        'mime'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function petition()
    {
        return $this->belongsTo(Petition::class);
    }

    /**
     * @return string
     */
    public function getFullPath()
    {
        return storage_path('app/images/' . $this->filename);
    }
}
