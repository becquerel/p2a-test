<?php

namespace App\Exceptions;


use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PetitionNotPublishedException extends NotFoundHttpException
{

}