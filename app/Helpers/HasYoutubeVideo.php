<?php

namespace App\Helpers;
use App\Exceptions\ObjectHasNoYoutubeVideoException;


/**
 * helper trait for parsing youtube video URL
 */
trait HasYoutubeVideo
{
    /**
     * @return string
     * @throws ObjectHasNoYoutubeVideoException
     */
    public function getYoutubeVideoId()
    {
        if (!isset($this->youtube_url)) {
            throw new ObjectHasNoYoutubeVideoException;
        }
        preg_match('/^https:\/\/www.youtube.com\/watch\?v=(.*)/i', $this->youtube_url, $matches);
        if (isset($matches[1])) {
            return $matches[1];
        }

        return '';
    }
}