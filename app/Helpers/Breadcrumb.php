<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 4/14/17
 * Time: 1:13 AM
 */

namespace App\Helpers;


class Breadcrumb
{
    /**
     * @var string
     */
    private $label;

    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $class;

    public function __construct(string $label, string $url = '', $class = '')
    {
        $this->label = $label;
        $this->url = $url;
        $this->class = $class;
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getClass(): string
    {
        return $this->class;
    }


}