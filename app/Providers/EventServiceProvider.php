<?php

namespace App\Providers;

use App\Events\PetitionWasSigned;
use App\Listeners\PetitionSignedListener;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    use DispatchesJobs;

    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        PetitionWasSigned::class => [
            PetitionSignedListener::class,
        ],
    ];

}
