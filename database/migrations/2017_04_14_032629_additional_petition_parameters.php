<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AdditionalPetitionParameters extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('petitions', function(Blueprint $table) {
            $table->text('thank_you_message');
            $table->text('thank_you_email');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('petitions', function(Blueprint $table) {
            $table->dropColumn('thank_you_message');
            $table->dropColumn('thank_you_email');
        });
    }
}
