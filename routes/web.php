<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;

//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();

// Everything is wrapped into router group, so no facades are used in custom routes, and we have full code-complete
Route::group([], function(Router $router) {
    // binds {petition} parameter so that it is automatically loaded from repository and injected as an instance,
    // instead of injecting ID and doing lookup inside controller methods
    $router->bind('petition', function ($id) {
        return app(\App\Repositories\PetitionRepository::class)->find($id);
    });
    $router->bind('image', function ($id) {
        return app(\App\Repositories\ImageRepository::class)->find($id);
    });

    $router->get('/', ['as' => 'home', 'uses' => 'FrontendPetitionController@index']);
    $router->get('/petition/{petition}/{slug?}', ['as' => 'petition.show', 'uses' => 'FrontendPetitionController@show']);
    $router->post('/petition/{petition}/sign', ['as' => 'petition.sign', 'uses' => 'FrontendPetitionController@sign']);
    $router->get('/petition/{petition}/{slug?}/thank-you', ['as' => 'petition.thankYou', 'uses' => 'FrontendPetitionController@thankYou']);
    $router->get('/petition/img/{petition}/{image}', ['as' => 'petition.image', 'uses' => 'FrontendPetitionController@image']);


    // routes for client account
    $router->group(['prefix' => 'client'], function (Router $router) {
        // home page after client login
        $router->get('/', ['as' => 'client.index', 'uses' => 'ClientController@index']);

        // petitions admin interface
        $router->group(['prefix' => 'petition'], function (Router $router) {
            $router->get('/', ['as' => 'client.petitions.index', 'uses' => 'PetitionController@index']);
            $router->get('/create', ['as' => 'client.petitions.create', 'uses' => 'PetitionController@create']);
            $router->post('/', ['as' => 'client.petitions.store', 'uses' => 'PetitionController@store']);
            $router->get('/{petition}', ['as' => 'client.petitions.show', 'uses' => 'PetitionController@show']);
            $router->get('/{petition}/edit', ['as' => 'client.petitions.edit', 'uses' => 'PetitionController@edit']);
            $router->put('/{petition}', ['as' => 'client.petitions.update', 'uses' => 'PetitionController@update']);
            $router->delete('/{petition}', ['as' => 'client.petitions.destroy', 'uses' => 'PetitionController@destroy']);

            $router->get('/toggle/{petition}', [
                'as' => 'client.petitions.togglePublished',
                'uses' => 'PetitionController@togglePublished'
            ]);
        });

    });
});


