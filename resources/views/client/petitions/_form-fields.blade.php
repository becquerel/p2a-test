<div class="col-md-12 col-sm-12">
    <div class="form-group inline-group group-to-right">
        {!! Form::label('title', 'Petition Title') !!}
        {!! Form::text('title', old('title', isset($petition) ? $petition->title : ''), ['class' => 'form-control', 'required']) !!}
        {!! $errors->first('title', '<span class="label label-danger">:message</span>') !!}
    </div>
</div>
<div class="col-md-12 col-sm-12">
    <div class="form-group inline-group group-to-right">
        {!! Form::label('summary', 'Petition Summary') !!}
        {!! Form::textarea('summary', old('summary', isset($petition) ? $petition->summary : ''), ['class' => 'form-control', 'required', 'rows' => '3']) !!}
        {!! $errors->first('summary', '<span class="label label-danger">:message</span>') !!}
    </div>
</div>
<div class="col-md-12 col-sm-12">
    <div class="form-group inline-group group-to-right">
        {!! Form::label('body', 'Petition Body') !!}
        {!! Form::textarea('body', old('body', isset($petition) ? $petition->body : ''), ['class' => 'form-control', 'required']) !!}
        {!! $errors->first('body', '<span class="label label-danger">:message</span>') !!}
    </div>
</div>
<div class="col-md-12 col-sm-12">
    <div class="form-group inline-group group-to-right">
        {!! Form::label('thank_you_message', 'Thank you message') !!}
        {!! Form::textarea('thank_you_message', old('thank_you_message', isset($petition) ? $petition->thank_you_message : 'Thank you for signing the petition'), ['class' => 'form-control', 'required',  'rows' => '3']) !!}
        {!! $errors->first('thank_you_message', '<span class="label label-danger">:message</span>') !!}
    </div>
</div>
<div class="col-md-12 col-sm-12">
    <div class="form-group inline-group group-to-right">
        {!! Form::label('thank_you_email', 'Thank you email') !!}
        {!! Form::textarea('thank_you_email', old('thank_you_email', isset($petition) ? $petition->thank_you_email : ''), ['class' => 'form-control', 'required', 'rows' => '3']) !!}
        {!! $errors->first('thank_you_email', '<span class="label label-danger">:message</span>') !!}
        <span class="text-muted">You can use <strong>{name}</strong> and <strong>{petition}</strong> placeholders in the message</span>
    </div>
</div>

<div class="col-md-12 col-sm-12">
    <div class="form-group inline-group group-to-right">
        {!! Form::label('youtube_url', 'Youtube URL') !!}
        {!! Form::text('youtube_url', old('youtube_url', isset($petition) ? $petition->youtube_url : ''), ['class' => 'form-control', 'placeholder' => 'https://www.youtube.com/watch?v=vSKGsmjbq2w']) !!}
        {!! $errors->first('youtube_url', '<span class="label label-danger">:message</span>') !!}
    </div>
</div>

<div class="col-md-8 col-sm-12">
    <div class="form-group inline-group group-to-right">
        {!! Form::label('photo', 'Photo Upload') !!}
        {!! Form::file('photo', ['class' => 'form-control']) !!}
        {!! $errors->first('photo', '<span class="label label-danger">:message</span>') !!}
        <span class="text-muted">JPG and PNG photos are allowed. Leave blank to keep current photo</span>
    </div>
</div>
@if(isset($petition))
    @foreach ($petition->images as $image)
        <div class="col-md-4 col-sm-12">
            <img class="img-responsive" src="{{ route('petition.image', ['petition' => $petition->id, 'image' => $image->id]) }}" />
        </div>
    @endforeach
@endif

<div class="col-md-12 col-sm-12">
    <div class="form-group inline-group group-to-right">
        {!! Form::label('is_published_0', 'Private') !!}
        {!! Form::radio('is_published', '0', isset($petition) ? !$petition->is_published: true, ['required', 'id' => 'is_published_0']) !!}
        &nbsp;
        {!! Form::label('is_published_1', 'Published', ['for' => 'is_published_1']) !!}
        {!! Form::radio('is_published', '1', isset($petition) ? $petition->is_published : false, ['required', 'id' => 'is_published_1']) !!}
        {!! $errors->first('body', '<span class="label label-danger">:message</span>') !!}
    </div>
</div>