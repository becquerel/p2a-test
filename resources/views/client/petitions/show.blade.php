@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3>Signatures for <strong>{{ $petition->title }}</strong></h3>
                        <span class="label label-success">{{ $petition->signatures->count() }} people signed</span>
                    </div>

                    <div class="panel-body">
                        <div class="list-group">
                            @foreach($petition->signatures as $signature)
                                <div class="list-group-item">
                                    <h4 class="list-group-item-heading">
                                        {{ $signature->name }}
                                    </h4>
                                    <p class="list-group-item-text">
                                        {{ $signature->email }}
                                        <br />
                                        {{ $signature->phone }}
                                    </p>
                                </div>
                            @endforeach
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
