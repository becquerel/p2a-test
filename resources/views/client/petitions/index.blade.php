@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        My Petitions
                        <a class="btn btn-primary btn-sm pull-right" href="{{ route('client.petitions.create') }}">add petition</a>
                    </div>

                    <div class="panel-body">
                        @include('partials.flash')
                        <table class="table table-condensed table-striped">
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th class="text-center">Published</th>
                                    <th class="text-center">Signatures</th>
                                    <th class="text-right">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($petitions as $petition)
                                    <tr>
                                        <td width="200">{{ $petition->title }}</td>
                                        <td width="20" class="text-center">
                                            <a href="{{ route('client.petitions.togglePublished', ['petition' => $petition->id]) }}">
                                                @if($petition->is_published)
                                                    <span class="label label-success">YES</span>
                                                @else
                                                    <span class="label label-danger">NO</span>
                                                @endif
                                            </a>
                                        </td>
                                        <td width="20" class="text-center">
                                            <a href="{{ route('client.petitions.show', ['petition' => $petition->id]) }}">
                                                {{ $petition->signatures()->count() }}
                                            </a>
                                        </td>
                                        <td width="20" class="text-right">
                                            <a href="{{ route('client.petitions.edit', ['petition' => $petition->id]) }}" class="btn btn-sm btn-default">edit</a>
                                            {!! Form::open(['route' => ['client.petitions.destroy', $petition->id], 'method' => 'DELETE', 'class' => 'pull-right']) !!}
                                                <button class="btn btn-danger btn-sm">delete</button>
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
