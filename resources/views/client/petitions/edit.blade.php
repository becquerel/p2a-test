@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Create New Petition
                    </div>

                    {!! Form::open(['route' => ['client.petitions.update', $petition->id], 'method' => 'put', 'enctype' => 'multipart/form-data']) !!}

                    <div class="panel-body">
                        @include('partials.flash')
                        @include('client.petitions._form-fields')
                    </div>

                    <div class="panel-footer">
                        <a class="btn btn-danger btn-sm" href="{{ route('client.petitions.index') }}">back</a>
                        {!! Form::submit('Save', ['class' => 'btn btn-success pull-right']) !!}
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
