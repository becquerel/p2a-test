@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Currently Open Petitions</div>
                <div class="panel-body table-striped">
                    <div class="list-group">
                        @foreach($petitions as $petition)
                            <a class="list-group-item" href="{{ route('petition.show', ['petition' => $petition->id, 'slug' => str_slug($petition->title)]) }}">
                                <strong>{{ $petition->title }}</strong>
                                <div>{!! nl2br($petition->summary) !!}</div>
                            </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
