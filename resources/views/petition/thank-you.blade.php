@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading text-center">
                        <h3>{{ $petition->title }}</h3>
                    </div>

                    <div class="panel-body text-center">
                        {!! nl2br($petition->thank_you_message) !!}
                        <br /><br />
                        <a class="btn btn-primary" href="{{ route('home') }}">Return to Homepage</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
