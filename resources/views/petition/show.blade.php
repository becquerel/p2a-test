@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading text-center">
                        <h3>{{ $petition->title }}</h3>
                    </div>

                    <div class="panel-body">
                        @if($petition->images)
                            @foreach($petition->images as $image)
                                <div class="text-center">
                                    <img class="img-responsive" src="{{ route('petition.image', ['petition' => $petition->id, 'image' => $image->id]) }}" />
                                </div>
                            @endforeach
                        @endif
                        {!! nl2br($petition->summary) !!}
                        @if($petition->youtube_url)
                            <div class="text-center">
                                <iframe width="100%" height="400" src="https://www.youtube.com/embed/{{ $petition->getYoutubeVideoId() }}?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                            </div>
                        @endif
                        {!! nl2br($petition->body) !!}
                    </div>
                    <div class="panel-footer">
                        <h4 class="text-center">Take Action: sign this petition</h4>

                        {!! Form::open(['route' => ['petition.sign', $petition->id], 'method' => 'POST']) !!}

                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="form-group inline-group group-to-right">
                                    {!! Form::label('name', 'Your name') !!}
                                    {!! Form::text('name', old('name'), ['class' => 'form-control', 'required']) !!}
                                    {!! $errors->first('name', '<span class="label label-danger">:message</span>') !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="form-group inline-group group-to-right">
                                    {!! Form::label('email', 'Email address') !!}
                                    {!! Form::text('email', old('email'), ['class' => 'form-control', 'required']) !!}
                                    {!! $errors->first('email', '<span class="label label-danger">:message</span>') !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="form-group inline-group group-to-right">
                                    {!! Form::label('phone', 'Cell Phone #') !!}
                                    {!! Form::text('phone', old('phone'), ['class' => 'form-control', 'required']) !!}
                                    {!! $errors->first('phone', '<span class="label label-danger">:message</span>') !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 text-center">
                                @include('partials.flash')
                                {!! Form::submit('Sign Petition', ['class' => 'btn btn-success']) !!}
                            </div>
                        </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
