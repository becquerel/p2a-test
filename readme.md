## Phone2Action development exercise

Simple petition system

#### Installation

`clone the repository`  
run `composer install`  
copy `.env.example` file to `.env` and change mysql connection parameters (and others if needed)    
run `php artisan key:generate`  
run `php artisan migrate`
run `php artisan serve` to start the app
